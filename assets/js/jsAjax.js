$('document').ready(function() {
    $('.msg_div').scrollTop(99999);
});

function escapeHtml(text) {
    return text.replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

$('#btn').click(function () {
    $.ajax({
        type: 'post',
        url: 'home/postchat',
        data: JSON.stringify({'text' : $('#txt').val()}),
        contentType: 'application/json',
        dataType: 'json',
        processData: false,
        success: function(response){

            if (response.status) {
                $('.msg_card_body .msg').append('<div class="img_cont_msg">' +
                    '<img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class="rounded-circle user_img_msg">' + '<p style="color: white">' + response.name + '</p>'   +
                    '</div>' +
                    '<div class="msg_cotainer">' +
                    '' + ' <span  class="msg_time">'+ response.time + '</span>' + escapeHtml(response.message) +
                    '</div>');

                $('.msg_div').scrollTop(99999);
                $('#txt').val('');

                // var audio = $('<audio />', {
                //     autoPlay : 'autoplay',
                //     // controls : 'controls'
                // });
                // $('<source />').attr('src', 'assets/audio/Sound_17211.mp3').appendTo(audio);
                // audio.appendTo('body');

            }
        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        },
        alert(message) {
            alert(555);
        }

    });
});

function updateChat(){
    $.ajax({
        type: 'post',
        url: 'home/postchat',
        dataType: 'json',
        processData: false,
        success: function(response){

            if (response.status) {
                if ($('.msg').children().last().children().text() !== response.time) {
                    $('.msg_card_body .msg').append('<div class="img_cont_msg">' +
                        '<img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class="rounded-circle user_img_msg">' + '<p style="color: white">' + response.name + '</p>'   +
                        '</div>' +
                        '<div class="msg_cotainer">' +
                        '' + ' <span class="msg_time">'+ response.time + '</span>' + escapeHtml(response.message) +
                        '</div>');

                    $('.msg_div').scrollTop(99999);
                    $('#txt').val('');

                    var audio = $('<audio />', {
                        autoPlay : 'autoplay',
                        // controls : 'controls'
                    });
                    $('<source />').attr('src', 'assets/audio/Sound_17211.mp3').appendTo(audio);
                    audio.appendTo('body');
                } else {
                    return false;
                }
            }
        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        },
        alert(message) {
            alert(555);
        }

    });
}

setInterval(function () {
    updateChat();
},3000);

$(".one_user").hover(
    function(){$(this).addClass('pointerCursor');},
    function(){$(this).removeClass('pointerCursor');}
);
$('.one_user').click(function () {
    $('.msg_content').removeClass('justify-content-center');

    var name = $(this).find('.active_name').text();

        $.ajax({
            type: 'post',
            url: 'home/show',
            data: JSON.stringify({'to_user': $(this).find('.active_name').text()}),
            contentType: 'application/json',
            dataType: 'json',
            processData: false,
            success: function(response){

                if (response.status) {
                    if ($('.to_card')){

                        $('.to_card').remove();

                    $('.msg_content').append('<div class="card to_card col-md-3 col-sm-3 col-lg-3" style="float: left; margin-right: 7px; position: absolute; right: 0px">\n' +
                    '<p class="close" style="position: absolute; right: 20px; color: white; width: 3px">X</p>                ' +
                    '<div class="card-header msg_head">\n' +
                    '                    <div class="d-flex bd-highlight">\n' +
                    '                        <div class="img_cont to_status">\n' +
                    '                            <img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class="rounded-circle user_img">\n' +
                    '                        </div>\n' +
                    '                        <div class="user_info">' +
                    '                        <span id="to_user">' + name + '</span>' +
                    '                        <p>' + $(this).find('p').text() + '</p>' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '                <div class="card-body to_msg_div">\n' +
                    '                    <div class="to_msg justify-content-start mb-4">\n' +
                    '                    </div>\n' +
                    '                </div>' +
                    '                    <div class="card-footer">' +
                    '                    <div class="input-group">\n' +
                    '                        <textarea id="to_txt" type="text" name="to_text" class="form-control type_msg" placeholder="Type your message..."></textarea>\n' +
                    '                        <div class="input-group-append">\n' +
                    '                            <input id="to_btn" type="submit" class="input-group-text send_btn" name="to_send" value="Sends">\n' +
                    '                        </div>\n' +
                    '                    </div></div></div>');

                    if (response.status_user == '1') {
                        $('.to_status').append('<span class="online_icon"></span>');
                    } else {
                        $('.to_status').append('<span class="offline_icon"></span>');

                    }

                    var i = 0;
                        $.each(response.data,function (key,value) {
                            // console.log(response.data[0[0]]);
                            $('.to_msg').append('<div class="img_cont_msg">' +
                                '                   <img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class=" img rounded-circle user_img_msg"><p id="name">' + response.data[i][0] + '</p>' +
                                '                </div>' +
                                '                <div class="msg_cotainer">' + escapeHtml(response.data[i][1]) +
                                '                <span class="msg_time">' + response.data[i][2] + '</span>' +
                                '                </div>');
                            i++;
                        });

                        $('.to_msg_div').scrollTop(99999);

                    }

                    $('.to_msg_div').scrollTop(99999);


                    $(".close").click(function(){
                        $('.to_card').remove();
                        $('.msg_content').addClass('justify-content-center');
                    });


                    $('#to_btn').click(function () {
                        $.ajax({
                            type: 'post',
                            url: 'home/tochat',
                            data: JSON.stringify({'text': $('#to_txt').val(),
                                'to_user': $('#to_user').text()}),
                            contentType: 'application/json',
                            dataType: 'json',
                            processData: false,
                            success: function(response){

                                if (response.status) {
                                    $('.to_msg_div .to_msg').append('<div class="img_cont_msg">' +
                                        '<img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class="rounded-circle user_img_msg">' + '<p style="color: white">' + response.name + '</p>'   +
                                        '</div>' +
                                        '<div class="msg_cotainer">' +
                                        '' + ' <span class="msg_time">'+ response.time + '</span>' + escapeHtml(response.message) +
                                        '</div>');

                                    $('.to_msg_div').scrollTop(99999);
                                    $('#to_txt').val('');

                                    var audio = $('<audio />', {
                                        autoPlay : 'autoplay',
                                    });
                                    $('<source />').attr('src', 'assets/audio/Sound_17211.mp3').appendTo(audio);
                                    audio.appendTo('body');

                                }
                            },
                            error: function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                            },
                            alert(message) {
                                alert(555);
                            }

                        });
                    });

                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            },
            alert(message) {
                alert(555);
            }

        });
    });

function updateToChat(){
    $.ajax({
        type: 'post',
        url: 'home/tochat',
        dataType: 'json',
        processData: false,
        success: function(response){

            if (response.status) {
                if ($('.to_msg').children().last().children().text() !== response.time) {
                    $('.to_msg_div .to_msg').append('<div class="img_cont_msg">' +
                        '<img src="https://devilsworkshop.org/files/2013/01/enlarged-facebook-profile-picture.jpg" class="rounded-circle user_img_msg">' + '<p style="color: white">' + response.name + '</p>'   +
                        '</div>' +
                        '<div class="msg_cotainer">' +
                        '' + ' <span class="msg_time">'+ response.time + '</span>' + escapeHtml(response.message) +
                        '</div>');

                    $('.to_msg_div').scrollTop(99999);
                    $('#to_txt').val('');

                    var audio = $('<audio />', {
                        autoPlay : 'autoplay',
                    });
                    $('<source />').attr('src', 'assets/audio/Sound_17211.mp3').appendTo(audio);
                    audio.appendTo('body');
                } else {
                    return false;
                }
            }
        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        },
        alert(message) {
            alert(555);
        }

    });
}

setInterval(function () {
    // updateToChat();
},3000);