var pass = document.querySelector('#pass');
var conf_pass = document.querySelector('#conf_pass');
var msg = document.querySelector('#message');

conf_pass.addEventListener('input', function () {

    if (pass.value !== conf_pass.value) {
        msg.style.color = 'red';
        msg.innerHTML = 'не совпадают';
    }
    else {
        msg.style.color = '#006400';
        msg.innerHTML = 'совпадают';
    }
});
