<?php

require_once(ROOT.'/models/ExtendsModel.php');

class ExtendsController
{
    public $extendsModel = null;

    public  function __construct() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function userStatus()
    {

        if($_SESSION) {
            $user_id = $_SESSION['user_id'];
            $extendsModel = new ExtendsModel();
            $extendsModel->updateStatusUser($user_id);
        }

    }
    public function redirect($url) {
        header("Location: /" . $url);
        exit();

    }

    public function render($page, $data=[]) {
        extract($data);
        require ROOT.'/views/'.$page.'.php';
    }

}