<?php

require_once (ROOT.'/models/Home.php');
require_once (ROOT.'/models/Chat.php');
require_once (ROOT.'/controllers/ExtendsController.php');
require_once (ROOT.'/models/MysqlMethods.php');


    class HomeController extends ExtendsController
    {
        private $homeModel = null;
        private $chatModel = null;
        private $mysqlMethods = null;


        public function __construct()
        {
            parent::__construct();
            $this->homeModel = new Home();
            $this->chatModel = new Chat();
            $this->mysqlMethods = new MysqlMethods();
            if (!$_SESSION['user_id']) {
                $this->redirect('login'); die();
            }
        }

        public function actionChat()
        {
            $user_name = $_SESSION['user_login'];
            $user_id = $_SESSION['user_id'];

            $data = [
                'id' => $user_id,
                'user_name' => $user_name,
                'status' => 1
            ];

          $this->homeModel->exInsert($data);
          $this->homeModel->exUpdate('activ_users', ['status' => 1], ['status' => 0, 'id' => $user_id]);

          $query = $this->homeModel->exSelect('user_name');

          $users = $this->mysqlMethods->sqlAll($query);

            foreach ($users as $value) {
                $res[] = implode($value);
            }

            $selectStatus = $this->homeModel->exSelect('status');

            $status = $this->mysqlMethods->sqlAll($selectStatus);

            $selectData = $this->chatModel->exSelect('user_name, chat_message, timestamp', ['from_user_id' => '0']);

            if ($selectData) {
                $allData = $this->mysqlMethods->sqlAll($selectData);

                if ($allData != null) {
                    foreach ($allData as $keys => $values) {
                        $fullData[] = $values;
                    }
                }else {
                    $fullData = [];
                }
            }

            $sendData['res'] = $res;
            $sendData['user_name'] = $user_name;
            $sendData['fullData'] = $fullData;
            $sendData['status'] = $status;

            $this->render('homePage', $sendData);

        }

        public function actionPostchat()
        {

            $json = file_get_contents('php://input');
            $result = json_decode($json, true);
            $textMessage = $result['text'];

            $data = [
                'chat_message' => $textMessage,
                'user_name' => $_SESSION['user_login'],
                'to_user_id' => $_SESSION['user_id'],
                'from_user_id' => '0',
            ];

            if ($textMessage !== null && $textMessage !== '') {
                $res = $this->chatModel->exInsert($data);
            }

            $selectData = $this->chatModel->exSelect('user_name, chat_message, timestamp');

            if ($selectData) {
                $allData = $this->mysqlMethods->sqlAll($selectData);

                if ($allData != null) {
                    foreach ($allData as $keys => $values) {
                        $fullData[] = $values;
                    }
                    foreach ($fullData as $key => $item) {
                        $response = $item;
                    }
                }else {
                    $fullData = [];
                }
            }

            if ($response) {
                echo json_encode([
                    'status' => true,
                    'name' => $response[0],
                    'message' => $response[1],
                    'time' => $response[2]
                ]);
            } else if (isset($json)) {
                echo json_encode([
                    'status' => true,
                    'name' => $response[0],
                    'message' => $response[1],
                    'time' => $response[2]
                ]);
            } else{
                echo json_encode([
                    'status' => false
                ]);
            }

            exit;

        }

        public function actionTochat() {

            $json = file_get_contents('php://input');
            $result = json_decode($json, true);
            $textMessage = $result['text'];
            $toUser = $result['to_user'];

            $selectId = $this->homeModel->exSelect('id', ['user_name' => $toUser]);
            $id = $this->mysqlMethods->sqlAll($selectId);

            foreach ($id as $value) {
                $toUserId = implode($value);
            }

            $data = [
                'chat_message' => $textMessage,
                'user_name' => $_SESSION['user_login'],
                'to_user_id' => $_SESSION['user_id'],
                'from_user_id' => $toUserId,
            ];

            if ($textMessage !== null && $textMessage !== '') {
                $res = $this->chatModel->exInsert($data);
            }

            $selectData = $this->chatModel->exSelect('user_name, chat_message, timestamp', ['to_user_id' => $_SESSION['user_id'], 'from_user_id' => $toUserId], 'OR', ['to_user_id' => $toUserId, 'from_user_id' => $_SESSION['user_id']]);

            if ($selectData) {
                $allData = $this->mysqlMethods->sqlAll($selectData);

                if ($allData != null) {
                    foreach ($allData as $keys => $values) {
                        $fullData[] = $values;
                    }
                    foreach ($fullData as $key => $item) {
                        $response = $item;
                    }
                }else {
                    $fullData = [];
                }
            }

            if ($res) {
                echo json_encode([
                    'status' => true,
                    'name' => $response[0],
                    'message' => $response[1],
                    'time' => $response[2]
                ]);
            } else {
                echo json_encode([
                    'status' => false
                ]);
            }

            exit;
        }

        public function actionShow() {

            $json = file_get_contents('php://input');
            $result = json_decode($json, true);
            $toUser = $result['to_user'];

            $selectId = $this->homeModel->exSelect('id', ['user_name' => $toUser]);
            $id = $this->mysqlMethods->sqlAll($selectId);

            foreach ($id as $value) {
                $toUserId = implode($value);
            }

           


            $selectData = $this->chatModel->exSelect('user_name, chat_message, timestamp', ['to_user_id' => $_SESSION['user_id'], 'from_user_id' => $toUserId], 'OR', ['to_user_id' => $toUserId, 'from_user_id' => $_SESSION['user_id']]);

            $selectStatus = $this->homeModel->exSelect('status', ['user_name' => $toUser]);

            $status = $this->mysqlMethods->sqlAll($selectStatus);

            if ($selectData) {
                $allData = $this->mysqlMethods->sqlAll($selectData);

                if ($selectData) {
                    echo json_encode([
                        'status' => true,
                        'status_user' => $status,
                        'data' => $allData
                    ]);
                } else {
                    echo json_encode([
                        'status' => false
                    ]);
                }

                exit;
            }
        }
    }