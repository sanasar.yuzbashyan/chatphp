<?php

    require_once(ROOT.'/models/User.php');
    require_once(ROOT.'/controllers/ExtendsController.php');
    require_once (ROOT.'/models/MysqlMethods.php');

class LoginController extends ExtendsController
    {
        private $userModel = null;
        private $mysqlMethods = null;

    public  function __construct(){
            parent::__construct();
            $this->userModel = new User();
            $this->mysqlMethods = new MysqlMethods();
    }

        public function actionRegistration()
        {

            if ($_SESSION['user_id']) {
                $this->redirect('home');
            }

            if($_SERVER['REQUEST_METHOD'] == 'GET') {

                $this->render('registrationPage');

            }

            elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $err = [];

                if (isset($_POST['submit']) && isset($_POST['user_login']) && isset($_POST['user_password']) && isset($_POST['user_confirm_password'])) {

                    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['user_login'])) {
                        $err[] = "Логин может состоять только из букв английского алфавита и цифр";
                    }

                    if (strlen($_POST['user_login']) < 3 or strlen($_POST['user_login']) > 30) {
                        $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
                    }

                    $data = [
                        'user_login' => $_POST['user_login'] ? $_POST['user_login'] : null,
                        'user_password' => md5(md5(trim($_POST['user_password']))) ? md5(md5(trim($_POST['user_password']))) : null,
                        'user_confirm_password' => md5(md5(trim($_POST['user_confirm_password']))) ? md5(md5(trim($_POST['user_confirm_password']))) : null,
                    ];

                    $issetLogin = $this->userModel->exSelect('user_id', ['user_login'=> $data['user_login']]);
                    $issetName = $this->mysqlMethods->sqlArray($issetLogin);

                    if ($data['user_password'] !== $data['user_confirm_password']) {
                        $err['error_pas'] = 'Пароли не совпадают!';
                        $this->render('registrationPage', $err);
                    }
                    if (!empty($issetName)) {
                        $err['error_name'] = "Пользователь с таким логином уже существует в базе данных!";
                        $this->render('registrationPage', $err);
                    }

                    unset($data['user_confirm_password']);

                    if (empty($err)) {

                        $res = $this->userModel->exInsert($data);

                        if ($res) {
                            $this->redirect('login');
                        }
                    }


                } else {
                    $err['error_validation'] = 'Не заполнены поля формы';
                    $this->render('registrationPage', $err);
                }
            }

            $extend = new ExtendsController();
            $extend->userStatus();
        }

        public function actionLogin()
        {
            if ($_SESSION['user_id']) {
            $this->redirect('home');
        }
            if ($_SERVER['REQUEST_METHOD'] == 'GET')
            {

                $this->render('loginPage');

            }

            elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                    if (isset($_POST['submit']) && isset($_POST['user_login']) && isset($_POST['user_password']))
                    {
                        $data = [
                            'user_login' => $_POST['user_login'] ? $_POST['user_login'] : null,
                            'user_password' => md5(md5(trim($_POST['user_password']))) ? md5(md5(trim($_POST['user_password']))) : null,
                        ];

                        $selectData = [
                            'user_login' => $data['user_login'],
                            'user_password' => $data['user_password']
                        ];

                        $query = $this->userModel->exSelect('*' , $selectData);
                        $id = $this->userModel->exSelect('user_id', $selectData);

                        $user_id = $this->mysqlMethods->sqlArray($id);
                        $numRows = $this->mysqlMethods->sqlRows($query);

                        if($numRows != 0)
                        {

                            while($row = $this->mysqlMethods->sqlAssoc($query))
                            {
                                $db_username=$row['user_login'];
                                $db_password=$row['user_password'];
                            }
                            if($data['user_login'] == $db_username && $data['user_password'] == $db_password)
                            {
                                $_SESSION['user_id'] = $user_id['user_id'];
                                $_SESSION['user_login'] = $data['user_login'];
                                $this->redirect('home');
                            }

                        }else{
                            $err['error_validation'] = 'Неверное имя пользователя или пароль!';
                            $this->render('loginPage', $err);
                        }

                    }

            }
            $extend = new ExtendsController();
            $extend->userStatus();

        }

        public function actionLogout() {
            unset($_SESSION['user_id']);
            $this->redirect('login'); die();
        }

    }