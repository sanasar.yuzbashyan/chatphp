<?php
/**
 * Created by PhpStorm.
 * User: sanasar
 * Date: 12/3/18
 * Time: 1:26 AM
 */

class Db
{
    public static function getConnection()
    {
        $paramPath = ROOT.'/config/db_params.php';
        $params = include($paramPath);

        $db = mysqli_connect ($params['host'], $params['user'], $params['password'],  $params['dbname']);
        if (!$db){
            die(mysqli_error());
        }
        return $db;
    }
}