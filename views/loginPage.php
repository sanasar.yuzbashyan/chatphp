<?php include ROOT.'/views/layouts/header.php'?>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin">
                <div class="card-body">
                    <?php if (isset($error_validation)) { ?>
                    <p class="signin"><?php echo $error_validation; }?></p>
                    <h5 class="card-title text-center">Sign In</h5>
                    <form class="form-signin" method="post" action="/login">
                        <div class="form-label-group">
                            <input type="text" name="user_login" id="name" class="form-control" placeholder="Name" required autofocus>
                            <label for="name">Name</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" name="user_password" id="inputPassword" class="form-control" placeholder="Password" required>
                            <label for="inputPassword">Password</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="submit">Sign in</button>
                    </form>
                </div>
                <a class="registration" href="/registration">Registration</a>
            </div>
        </div>
    </div>
</div>
<?php include ROOT.'/views/layouts/footer.php'?>


