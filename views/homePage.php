<?php include ROOT.'/views/layouts/header.php'?>
<link rel="stylesheet" type="text/css" href="/assets/css/homePageStyle.css">
<div class="container-fluid">
    <div class="row justify-content-center h-100 msg_content">
        <div class="col-md-4 col-xl-3 col-sm-2 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
                <div class="card-header">
                    <div class="input-group">
                        <input type="text" placeholder="Search..." name="" class="form-control search">
                        <div class="input-group-prepend">
                            <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="card-body contacts_body">
                    <ui class="contacts">
                        <?php foreach ($res as $item) { ?>
                        <li class="active">
                            <div class="one_user d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="/assets/images/image1.jpg" class="rounded-circle user_img">
                                    <?php $userStatus = array_shift($status); $userStatus = implode($userStatus);   if ($userStatus == '1') {  ?>
                                    <span class="online_icon"></span>
                                    <?php } else { ?>
                                    <span class="offline_icon"></span>
                                    <?php  }; ?>
                                </div>
                                <div class="user_info">
                                    <span class="active_name"><?php echo $item; ?></span>
                                    <?php if ($userStatus == '1') { ?>
                                    <p><?php echo $item; ?> is online</p>
                                    <?php } else { ?>
                                    <p><?php echo $item; ?> is offline</p>
                                    <?php } ?>
                                </div>
                        </li>
                        <?php } ?>
                    </ui>
                </div>
                <div class="card-footer"></div>
            </div></div>
        <div class="col-md-8 col-xl-6 chat">
            <div class="card">
                <div class="card-header msg_head">
                    <div class="d-flex bd-highlight">
                        <div class="img_cont">
                            <img src="/assets/images/image1.jpg" class="rounded-circle user_img">
                            <span class="online_icon"></span>
                        </div>
                        <div class="user_info">
                            <span>Chat with <?php echo $user_name; ?></span>
                            <p>1767 Messages</p>
                        </div>
                        <div class="video_cam">
                            <span><i class="fas fa-video"></i></span>
                            <span><i class="fas fa-phone"></i></span>
                        </div>
                    </div>
                    <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                    <div class="action_menu">
                        <ul>
                            <li><i class="fas fa-user-circle"></i> View profile</li>
                            <li><i class="fas fa-users"></i> Add to close friends</li>
                            <li><i class="fas fa-plus"></i> Add to group</li>
                            <li><i class="fas fa-ban"></i> Block</li>
                        </ul>
                    </div>
                </div>
                <div class="card-body msg_card_body msg_div">
                    <div class="msg justify-content-start mb-4">
                        <?php if ($fullData) { foreach ($fullData as $key => $items) { ?>
                                <div class="img_cont_msg">
                                <img src="/assets/images/image1.jpg" class=" img rounded-circle user_img_msg"><p id="name"><?php echo $items[0]; ?></p>
                            </div>
                        <div class="msg_cotainer"><?php echo htmlspecialchars($items[1]);  ?>
                            <span class="msg_time"><?php echo $items[2]; ?></span>
                        </div>
                        <?php  } } ?>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="input-group">
                        <textarea id="txt" type="text" name="text" class="form-control type_msg" placeholder="Type your message..."></textarea>
                        <div class="input-group-append">
                            <input id="btn" type="submit" class="input-group-text send_btn" name="send" value="Sends">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include ROOT.'/views/layouts/footer.php'?>
