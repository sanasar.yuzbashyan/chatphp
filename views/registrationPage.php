<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/registrPageStyle.css">

</head>
<body>
<div class="main-w3layouts wrapper">
    <h1>Creative SignUp Form</h1>
    <div class="main-agileinfo">
        <?php if (isset($error_pas)) { ?>
        <p class = "error_pas"><?php echo  $error_pas; }else if (isset($error_name)) {?></p>
        <p class = "error_pas"><?php echo  $error_name; } else if (isset($error_pas) && isset($error_name)) { ?></p>
        <p class = "error_pas"><?php echo  $error_name . '</br>' . $error_pas; } else if (isset($error_validation)) { ?></p>
        <p class = "error_pas"><?php echo  $error_validation; } ?></p>
        <div class="agileits-top">
            <form action="/registration" method="post">
                <input class="text" type="text" name="user_login" placeholder="Username" required="">
                <input class="text" type="password" name="user_password" placeholder="Password" id="pass" required="">
                <input class="text" type="password" name="user_confirm_password" placeholder="Confirm Password" id="conf_pass" required="">
                <span id='message'></span>
                <input id="btn" type="submit" name="submit" value="SIGNUP">
            </form>
            <p>Don't have an Account? <a href="/login"> Login Now!</a></p>
        </div>
    </div>
    <!-- copyright -->
    <div class="colorlibcopy-agile">
        <p>© 2018 Colorlib Signup Form. All rights reserved | Design by <a href="https://colorlib.com/" target="_blank">Colorlib</a></p>
    </div>
    <!-- //copyright -->
    <ul class="colorlib-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>

<script src="/assets/js/jquery%203.3.1.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/script.js"></script>
</body>
</html>