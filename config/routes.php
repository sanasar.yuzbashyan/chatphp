<?php

    return array(
        'home/show' => 'home/show',
        'home/tochat' => 'home/tochat',
        'home/postchat' => 'home/postchat',
        'home' => 'home/chat',
        'registration' => 'login/registration',
        'error' => 'error/fail',
//        '' => 'login/login',
        'logout' => 'login/logout',
        'login' => 'login/login'
    );