<?php

require_once(ROOT . '/components/Db.php');

class MainModel
{
    private $db;
    public $mysqlMethods = null;

    public function __construct()
    {
        $this->db = Db::getConnection();
    }

    public function insert($values, $columns = null)
    {

        $insert = 'INSERT INTO ' . $this->table;
        if ($columns != null) {
            $insert .= ' (' . $columns . ')';
        }
        if (gettype($values) === array()) {
            for ($i = 0; $i < count($values); $i++) {
                if (is_string($values[$i]))
                    $values[$i] = '"' . $values[$i] . '"';
            }
            $values = implode(',', $values);
            $insert .= ' VALUES (' . $values . ')';
        }

        $insert .= ' VALUES (' . $values . ')';
        $query = mysqli_query($this->db, $insert);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }


    public function exSelect($columns = '*', $conditions = null, $or = null, $condition = null, $order = null)
    {
        $where = '';
        $orWhere = '';

        if (!empty($conditions)) {
            foreach ($conditions as $key => $value) {
                $where .= "AND " .  $key . " = '" . $value . "' ";
            }
            $where = trim($where, 'AND');
            $where = 'WHERE '. '(' . $where . ')';
        }
        $select = "SELECT $columns FROM  $this->table  $where";

        if ($or != null && !empty($condition)) {
            $select .= ' OR ';
            foreach ($condition as $key => $value) {
                $orWhere .= "AND " .  $key . " = '" . $value . "' ";
            }
            $orWhere = trim($orWhere, 'AND');
            $select .=  '(' . $orWhere . ')';
        }

        if ($order != null) {
            $select .= ' ORDER BY ';
            $select .= $where;
        }

        $query = mysqli_query($this->db, $select);
        if($query){
            return $query;
        }
        else{
            return false;
        }
    }


    public function exInsert($data)
    {
        $keysArray = array_keys($data);
        $columns = implode(', ', $keysArray);

        $valuesArray = array_values($data);
        $values = implode("', '", $valuesArray);
        $values = "'" . $values . "'";

        $insert = 'INSERT INTO ' . $this->table . ' (' . $columns . ') VALUES ('. $values .')';
        $query = mysqli_query($this->db, $insert);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function exUpdate($table, $rows, $conditions)
    {
       $where = '';
       $set = '';

        foreach ($conditions as $key => $value) {
            $where .= "AND " .  $key . " = '" . $value . "' ";
        }
        $where = trim($where, 'AND');
        $where = ' WHERE '. $where;
        $update = 'UPDATE ' . $table . ' SET ';

        foreach ($rows as $key => $value) {
            $set .= $key . ' = ' . $value;
        }

        $update .= $set . $where;
        $query = mysqli_query($this->db, $update);
        if ($query) {
            return true;
        } else {
            return false;
        }

    }

    public function delete($table, $where = null)
    {

        if ($where == null) {
            $delete = 'DELETE ' . $table;
        } else {
            $delete = 'DELETE FROM ' . $table . ' WHERE ' . $where;
        }
        $query = mysqli_query($this->db, $delete);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}