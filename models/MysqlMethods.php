<?php

class MysqlMethods
{

    public $numRows = '';
    public $fetchArray = [];
    public $fetchAssoc = [];
    public $fetchAll = [];
    public $fetchObject = [];

    public function sqlRows($query)
    {
        $this->numRows = mysqli_num_rows($query);
        return $this->numRows;
    }

    public function sqlArray($query)
    {
        $this->fetchArray = mysqli_fetch_array($query);
        return $this->fetchArray;
    }

    public function sqlAssoc($query)
    {
        $this->fetchAssoc = mysqli_fetch_assoc($query);
        return $this->fetchAssoc;
    }

    public function sqlAll($query)
    {
        $this->fetchAll = mysqli_fetch_all($query);
        return $this->fetchAll;
    }

    public function sqlObject($query)
    {
        $this->fetchObject = mysqli_fetch_object($query);
        return $this->fetchObject;
    }

}